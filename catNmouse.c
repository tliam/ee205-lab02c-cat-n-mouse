///////////////////////////////////////////////////////////////////////////////
//           University of Hawaii, College of Engineering
/// @brief   Lab02c - Cat `n Mouse - EE 205 - Spr 2022
///
/// This is a classic "I'm thinking of a number" guessing game.  The mouse
/// will think of a number... and the cat will keep trying to guess it.
///
/// @file    catNmouse.c
/// @version 1.0 - Initial version
///
/// Compile: $ gcc -o catNmouse catNmouse.c
///
/// Usage:  catNmouse [n]
///   n:  The maximum number used in the guessing game
///
/// Exit Status:
///   1:  The command line parameter was not a valid value
///   0:  The cat finally guessed correctly
///
/// Example:
///   $ ./catNmouse 2000
///   OK cat, I'm thinking of a number from 1 to 2000.  Make a guess:  1385
///   No cat... the number I’m thinking of is smaller than 1385
///   OK cat, I'm thinking of a number from 1 to 2000.  Make a guess:  1382
///   No cat... the number I’m thinking of is larger than 1382
///   OK cat, I'm thinking of a number from 1 to 2000.  Make a guess:  1384
///   You got me.
///   |\---/|
///   | o_o |
///    \_^_/
///   
/// @author  Liam Tapper <tliam@hawaii.edu>
/// @date    23/01/2022
///////////////////////////////////////////////////////////////////////////////


#include <stdio.h>
#include <stdlib.h>
#include <time.h>
int main( int argc, char* argv[] ) {
   srand(time(0));
   printf( "Cat `n Mouse\n" );
   //printf( "The number of arguments is: %d\n", argc );
   int guessRange = -1;

   if (argv[1] != NULL)
      guessRange = atoi( argv[1] );

   while(argv[1] != NULL && (guessRange <= 0)){
      printf("Please enter a number that is greather than 0\n");
      scanf("%d", &guessRange);
   }

   // This is an example of getting a number from a user
   // Note:  Don't enter unexpected values like 'blob' or 1.5.  
   int aGuess;
   int DEFAULT_MAX_NUMBER = 2048;
   int valueToGuess;
   //sets the max range depending on the input in the command line
   if (guessRange == -1)
      valueToGuess = (rand() % DEFAULT_MAX_NUMBER)+1;
   else
      valueToGuess = (rand() % guessRange)+1;
      
 
   //printf("VALUE TO GUESSi %d\n\n", valueToGuess);
         //Informing player of guess range depending on input on command line
      if (guessRange == -1)
         printf( "Okay cat, I'm thinking of a number between 1 and %d\n", DEFAULT_MAX_NUMBER);
      else
          printf( "Okay cat, I'm thinking of a number between 1 and %d\n", guessRange);



   do{
      scanf( "%d", &aGuess);

      //reset if out of range
      if (aGuess > guessRange && guessRange != -1){
         printf("Value must be less than %d\n", guessRange);
         continue;
      }else if (aGuess > DEFAULT_MAX_NUMBER && guessRange == -1){
         printf("Value must be less than %d\n", DEFAULT_MAX_NUMBER);
         continue;
      }else if (aGuess < 1){
         printf("Value must be > 1\n");
         continue;
      }

      //now check to see if the number is higher/lower than the number to guess
      if (aGuess > valueToGuess){
         printf("No cat ... the number I am thinking of is smaller than %d\n", aGuess);
         continue;
      }
      else if (aGuess < valueToGuess){
         printf("No cat ... the number I am thinking of is larger than %d\n", aGuess);
         continue;
      }

    //  printf( "The number was [%d]\n", aGuess );
        printf("You got me!\n");
        printf(" |\\__/,|   (`\\ \n |_ _  |.--.) )\n ( T   )     /\\ \n(((^_(((/(((_/\n");
        break;
   } while(aGuess != valueToGuess);
   return 0;  // This is an example of how to return a 1 : return 0 (changed it to one to exit program)
   printf("END\n");
}

